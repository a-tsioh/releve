name := "releve-scala"

version := "0.1"

scalaVersion := "2.12.10"

organization := "fr,magistry.nlp"

libraryDependencies ++= Seq(
  "fr.magistry.nlp" %% "eleve-core" % "0.1-SNAPSHOT"
)
