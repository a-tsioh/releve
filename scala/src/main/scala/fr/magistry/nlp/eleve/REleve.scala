package fr.magistry.nlp.eleve

import fr.magistry.nlp.eleve.scripts.Chinese.CJKSeq

class REleve(order: Int) {
  private val e = new Eleve[String](order)

  def trainOnFile(file: String ) = {
    val src = io.Source.fromFile(file)
    e.train(src.getLines().toList.map(_.toSeq.map(_.toString)),1)
    src.close()
  }

  def train(data: Array[String]): Unit = {
    e.train(data.map(_.toSeq.map(_.toString)), depth = 1)
  }

  def tokenize(s: String): Array[String] = {
    scripts.Chinese.tokenize(s).map { tokseq =>
      tokseq.tokens.flatMap {
        case CJKSeq(cjks) => cjks.map(_.c)
        case x => Seq(x.str)
      }
        .toArray
    }
      .getOrElse(Array.empty)
  }

  def trainCJK(data: Array[String]): Unit = {
    val trainingData = data.map { line =>
      tokenize(line).toList
    }
    e.train(trainingData, 1)
  }

  def segmentCJK(line: String): Array[String] = {
    scripts.Chinese.tokenize(line).map {tokseq =>
      tokseq.tokens.flatMap {
        case CJKSeq(cjks) =>
          val inputs = cjks.map(_.c).toArray
          e.segmente(inputs).getWords(inputs).map(_.mkString(""))
        case x => Seq(x.str)
      }.toArray
    }.getOrElse(Array.empty)
  }


  def getWords(input: String, threshold: Double): Array[String] = {
    val words =
      e
        .extractAutonomousChunks(input.toCharArray.map(_.toString), threshold)
        .map(_ mkString "")
    words.toArray
  }

  def segment(line: String): Array[String] = {
    val inArray = line.toCharArray.map(_.toString)
    val result = e.segmente(inArray)
    result.getWords(inArray).map(_.mkString("")).toArray
  }

}
